---

## **Activity 01 - Using the Ansible Commands**

---

1. To list all available modules use -

```bash
ansible-doc -l
```

2. To display only specific modules having a search term use the grep utility -

```bash
ansible-doc -l | grep copy 
```

3. To view the documentation of a specific module use -

```bash
ansible-doc copy
```

4. To run an ad-hoc command that performs simple tasks only once, use the syntax.

```bash
ansible hostORgroup -m module_name -a "arguments"
```

5. To get information about the network, machine, or metadata of the infrastructure, use the **`setup`** module -

```bash
ansible target -m setup
```

**NOTE**: **`target`** is name of the group configured in **`/etc/ansible/hosts`** file.
 
6. Use the respective operating system packaging module for installing any package; for entire lab activities, it is going to be the **`yum`** module.

[REFERENCE LINK - Operating System Packaging Modules](https://docs.ansible.com/ansible/latest/modules/list_of_packaging_modules.html#os)

To install apache web server use -

```bash
ansible target -m yum -a "name=httpd state=latest"
```
```bash
ansible target -m yum -a "name=httpd state=installed"
```

**NOTE**: To install a new package **`sudo`** privileges are required, otherwise an error is send back. 

Use **`--become`** options to gain privilege escalation as sudoer.

```bash
ansible target --become -m yum -a "name=httpd state=latest update_cache=yes"
```

7. To control services on remote hosts use the **`service`** module (for windows hosts use **`win_service`**). To start the apache service now, and after every re-boot use -

```bash
ansible target --become -m service \
-a "name=httpd enabled=yes state=started"
```

8. Open a new browser tab window to test apache web server is running on the target machine.

```bash
http://<TARGET-IP-ADDRESS>
```

9. To modify a single line in file you use **`lineinfile`** module -

```bash
ansible target --become -m lineinfile \
-a "path='/etc/httpd/conf/httpd.conf' regexp='^Listen ' insertafter='^#Listen ' line='Listen 8080' state=present"
```

10. To restart the apache web server using **`service`** module; to listen on new configured port number use -
 
```bash
ansible target --become -m service \
-a "name=httpd state=restarted"
```

11. Open a new browser tab window to test apache web server is running on the target machine.

```bash
http://<TARGET-IP-ADDRESS>:8080
```

12. To stop the service use -

```bash
ansible target --become -m service \
-a "name=httpd state=stopped"
```

Try refreshing the browser window and check that apache web-server cannot be reached.

13.  Use the respective operating system packaging module for removing any package; to remove apache web server use -

```bash
ansible target --become -m yum \
-a "name=httpd state=absent autoremove=yes"
```

[Reference Link - ansible cli command](https://docs.ansible.com/ansible/latest/cli/ansible.html)

---

## **Activity 02 - Using Ansible Playbook file**

---

1. To run any playbook use the syntax -

```bash
ansible-playbook <your-playbook-file.yml>

# to check only the syntax
ansible-playbook <your-playbook-file.yml> --syntax-check
```

**NOTE**: Use the **`--syntax-check`** option for any syntax error in the playbook file.

2. To clone the repository using git use - 

```bash
git clone https://gitlab.com/sujalmitra/ansible-activity.git 
```

3. To move to **`ansible-playground/lab-files/playbook-demo`** directory use -

```bash
cd ~/ansible-activity/lab-files/playbook-demo
```

4. Open the **`playbook-1.yml`** file and complete it based on the instructions given below -

i. In the hosts section, specify the node to install the apache web server.

ii. In the tasks section,

* Add the values to install apache web server
* Copy the source file (i.e. index.html in the same directory) to destination
* Start the service

5. To validate the syntax of your playbook file use -

```bash
ansible-playbook playbook-1.yml --syntax-check
```

6. To execute the tasks defined in ansible-playbook use -

```bash
ansible-playbook playbook-1.yml
```

7. There are two ways to check, if apache web server is running -

i. Open a new browser tab window and use the address -

```bash
http://<NODE-IP-ADDRESS>
```

ii. Use the curl command on the control node to see the web page content on console -

```bash
curl http://<NODE-IP-ADDRESS>
```

---

## **Activity 03 - Using Conditional and Handlers in Ansible Playbook file**

---

1. Open the **`playbook-2.yml`** file and complete it based on the instructions given in the file and below steps -

2. In the variables section, give the port number as 8080.

```yml
  vars:
    http_port: 8080
```

3. Add the information in the task `Changing the apache web server port number` as given below -

```yml
- name: Changing the apache web server port number
  lineinfile:
    path: /etc/httpd/conf/httpd.conf
    regexp: "^Listen "
    insertafter: "^#Listen "
    line: "Listen {{ http_port }}"
  when: http_port is defined
  notify:
  - Restart Apache
```

**NOTE**: **`{{ }}`** is used for accessing the variable value.

4. Add the information in the handler, which matches the notify, as given below - 

```yml
  handlers:
  - name: Restart Apache
    service:
      name: httpd
      state: restarted
```

5. To check the syntax and execute the ansible playbook file use -

```bash
# to check the syntax
ansible-playbook playbook-2.yml --syntax-check
# to execute the playbook
ansible-playbook playbook-2.yml
```

6. Use the address given below on a new browser tab window, to check that your web server is now listening on port 8080.

```bash
http://<NODE-IP-ADDRESS>:8080
```

**NOTE**: The apache web server port number is changed to `8080`, so that request is forwarded to this web server using a reverse proxy, which runs on port `80`.

8. To get the value of **`ansible_distribution_major_version`** fact variable value use -

```bash
ansible target -m setup | grep ansible_distribution_major_version
```

9.  Open the **`playbook-3.yml`** file and complete it based on the instructions given in the file.

10. To check the syntax and execute the ansible playbook file use -

```bash
# to check the syntax
ansible-playbook playbook-3.yml --syntax-check
# to execute the playbook
ansible-playbook playbook-3.yml
```

11.  Use the address given below on a new browser tab window, to check that your nginx server is now listening on port 80.

```bash
http://<NODE-IP-ADDRESS>
```

---

## **Activity 04 - Using the Jinja2 templating engine in Ansible Playbook**

---

1. To change to the **`jinja2-demo`** directory and check the content of directory use -

```bash
cd ~/ansible-playground/lab-files/jinja2-demo

ls 
```

2. Observe the **`index.j2`** file.

3. Open the **`playbook-1.yml`** file and provide the values as instructions given in the file. 

**NOTE**: **`copy`** module is replaced with **`template`** module that uses jinja2 templating engine of python to insert dynamic values in the file.

4. To check the syntax and execute the ansible **`playbook-1.yml`** file use -

```bash
# to check the syntax
ansible-playbook playbook-1.yml --syntax-check
# to execute the playbook
ansible-playbook playbook-1.yml
```

5. Use the address given below on a new browser tab window, to check that the home page display the greeting value provided.

```bash
http://<NODE-IP-ADDRESS>:8080
```

6. Observe the simple proxy configuration inside **`default.j2`** file and note the variables used inside it. 

7. Open the **`playbook-2.yml`** file, and in the vars section of the file, define the variables used in **`default.j2`**.

8. To execute the **`playbook-2.yml`** file use -

```bash
# to check the syntax
ansible-playbook playbook-2.yml --syntax-check
# to execute the playbook
ansible-playbook playbook-2.yml
```

9. Try to access the page using the below address, where nginx is used as reverse proxy; we would get a **`502 Bad Gateway`** due to SELinux policy of RedHat family.

```bash
http://<NODE-IP-ADDRESS>
```

10. To enable the access we need to add one more task using the **`seboolean`** module specifically for RedHat O.S. family before the handlers section in your existing **`playbook-1.yml`** file

```bash
  - name: Set httpd_can_network_connect flag on and keep it persistent across reboots
    seboolean:
      name: httpd_can_network_connect
      state: yes
      persistent: yes
```

11. The **`seboolean`** module is dependent on one more package **`libsemanage-python`** that must be present. To demo multiple packages installation in a single task using loops, modify the **`Install Apache Web Server`** task as show below  -

```bash
  - name: Install Apache Web Server and libsemanage-python package
    yum:
      name: "{{ item  }}"
      state: latest
      update_cache: yes
    with_items:
    - httpd
    - libsemanage-python
```

12. To execute the **`playbook-1.yml`** file use -

```bash
# to check the syntax
ansible-playbook playbook-1.yml --syntax-check
# to execute the playbook
ansible-playbook playbook-1.yml
```

13.  Try to access the page once again using the below address, this time you will be able to access the apache web server via proxy server.

```bash
http://<NODE-IP-ADDRESS>
```

---

## **Activity 05 - Working with multiple playbook files.**

---

1. To change to the **`multi-playbook-demo`** directory and check the content of directory use -

```bash
cd ~/ansible-playground/lab-files/multi-playbook-demo

ls 
```

2. Observe the different files in the directory.

3. Open the **`site.yml`** file and provide the values as instructions given in the file. 

4. To check the syntax and execute the ansible **`site.yml`** file use -

```bash
# to check the syntax
ansible-playbook site.yml --syntax-check
# to execute the playbook
ansible-playbook site.yml
```

5.  Try to access the page once again using the below address, this time you will be able to see the new greeting value, which is passed in **`site.yml`**.

```bash
http://<NODE-IP-ADDRESS>
```

---

## **Activity 06 - Working with Ansible Roles.**

---

1. To change to the **`roles-demo`** directory use -

```bash
cd ~/ansible-playground/lab-files/roles-demo
```

2. To refer the directory and files content in **`webserver`** roles directory use -

```bash
tree roles/webserver
```

3. To check the syntax and execute the ansible **`site.yml`** file use -

```bash
# to check the syntax
ansible-playbook site.yml --syntax-check
# to execute the playbook
ansible-playbook site.yml
```

**NOTE**: **`webserver`** roles is already created.

4. To create a new proxy server roles use -

```bash
ansible-galaxy role init --init-path roles proxy
```

5. Open the file **`roles/proxy/vars/main.yml`** and add the variables specific to proxy server inside it.

6. Open the file **`roles/proxy/handlers/main.yml`** and add the handler specific to proxy server inside it.

7. Open the file **`roles/proxy/tasks/main.yml`** and add the tasks specific to proxy server inside it.

8. To copy the file **`default.j2`** from **`multi-playbook-demo`** folder use -

```bash
cp ../multi-playbook-demo/default.j2 roles/proxy/templates/default.j2
```

9. Modify the **`site.yml`** in **`roles-demo`** directory as given

10. To check the syntax and execute the ansible **`site.yml`** file use -

```bash
# to check the syntax
ansible-playbook site.yml --syntax-check
# to execute the playbook
ansible-playbook site.yml
```

**NOTE**: You can change the greeting in **`roles/webserver/vars/main.yml`** to check that everything is working as expected.

11.   Try to access the page once again using the below address, everything should work as expected

```bash
http://<NODE-IP-ADDRESS>
```

**ADDITIONAL NOTES**

---

**default** - If a variable is defined nowhere else, the definition given in `defaults/main.yml` will be used.

**files and templates** - Contains affiliated files and Ansible templates (respectively) that are used within the role which Ansible checks automatically it does not need a path for resources stored in them when working in the role.

**handlers** - The handlers directory is used to store Ansible `handlers`.

**meta** - The meta directory contains authorship information which is useful if you choose to publish your role on **`galaxy.ansible.com`**.

**tasks** - The task directory is where most of your role will be written.

**tests** - The test directory contains a sample inventory and a **`test.yml`** playbook.

**vars** - create variable files that define necessary variables for your role.

---

## **DO IT YOURSELF**

---

In this activity, you have to uninstalled the apache web server. 

**HINTS**:

1. To check that service is running use -

```yml
tasks:
- name: To check apache service is running
  command: service httpd status
    warn=False
  register: result
  ignore_errors: yes
  when: ansible_os_family == 'RedHat'

- name: Debugging the result variable value
  debug:
    var: result
```

2. Stop the apache webserver service using the `service` module only when the `result is succeeded`

3. Remove the apache webserver service using the yum module.

4. Remove the directories created by apache web server using `file` module

```bash
# directories to remove (you can use with_items here)
/etc/httpd
/var/log/httpd
/var/www/html
```

---

## **Activity 07 - Using existing roles from Ansible Galaxy**

---

1. Refer the PDF file [here](https://innersource.accenture.com/users/khozema.nullwala/repos/ansible-playground/browse/lab-files/Activity_07.pdf) for this activity. 

---

## **Activity 08 - Using Ansible Vault**

---

1. To change to the **`vault-demo`** directory use -

```bash
cd ~/ansible-playground/lab-files/vault-demo
```

2. Download a new role for postgresql database from https://galaxy.ansible.com

```bash
ansible-galaxy install geerlingguy.postgresql
```

3. Most of the times when sensitive or confidential data need to be protected in the playbook, then it can be encrypted rather than just keeping it in a text file which is readable by all. Ansible Vault allows you to encrypt the playbook to protect the confidential data.

To create a new file encrypted with vault use -

```bash
ansible-vault create vars/main.yml
```

4. When the file opens, modify it with content below -

```yml
--- 
postgresql_databases: 
- name: example_db 
postgresql_users: 
- name: admin 
  password: supersecure
```

5. To view the content inside the **`vars/main.yml`** file use –

```bash
cat vars/main.yml
```

6. To view the actual content of the file without editing it or writing to the filesystem use -

```bash
ansible-vault view vars/main.yml
```

7. To edit an encrypted files use:

```bash
ansible-vault edit vars/main.yml
```

8. To decrypt a vault encrypted file use -

```bash
ansible-vault decrypt vars/main.yml
```

9. To encrypt an existing file use -

```bash
ansible-vault encrypt vars/main.yml
```

10. To change the password of an encrypted file use -

```bash
ansible-vault rekey vars/main.yml
```

11.  To use the encrypted file with playbook during execution, use the flag **`--ask-vault-pass`** -

```bash
ansible-playbook --ask-vault-pass site.yml
```

12.  From your node instance, access `postgresql` database using –

```bash
psql --host=localhost --dbname=example_db --username=admin --password
```

13. Type the \q to quit `postgresql` on node instance.

---

## **Activity 09 - Asynchronous Actions and Polling**

---

By default, tasks in playbooks block, meaning the connections stay open until the task is done on each node. This may not always be desirable, or you may be running operations that take longer than the SSH timeout.

To avoid blocking or timeout issues, you can use `asynchronous` mode to run all your tasks at once and then poll until they are done.

To launch a task asynchronously, specify its maximum runtime and how frequently you would like to poll for status. The default poll value is 10 seconds if you do not specify a value for poll.

1. To change to the **`async-demo`** directory use -

```bash
cd ~/ansible-playground/lab-files/async-demo
```

2. On the node instance type in the below command to keep a watch on the connection –

```bash
watch -d -n 1 who
```

3. To execute the ansible playbook and watch how the connection is stabilized till the task is not completed use -

```bash
ansible-playbook playbook.yml
```

4. Modify the playbook.yml task as shown below –

```yml
  tasks:
  - name: Simulating a long running operation for 120 sec, keep it alive till 150 seconds and poll interval is set at 5 seconds
    command: /bin/sleep 120
    async: 150
    poll: 5
```

5. Execute the ansible playbook using the command once again and observe the node instance. Due to polling the connection is established in an interval of 5 seconds -

```bash
ansible-playbook playbook.yml
```
